import {
    BarChart,
    Bar,
    Legend,
    XAxis,
    YAxis,ResponsiveContainer
  } from 'recharts';
  import "./VaccinationCoverage.css"
const VaccinationCoverage=(props)=>{
    const data = props.data

    const DataFormatter = (number) => {
        return number.toString();
      };
    return(
        <div className='vaccinationCoverageContainer'>
            <h1 className='headingVaccination'>Vaccination Coverage</h1>
        <ResponsiveContainer width="100%" height={500}>
          <BarChart
            data={data}// Corrected the data mapping
         
            margin={{
              top: 5,
            }}
          >
            <XAxis
              dataKey="vaccine_date"
              tick={{
                stroke: "gray",
                strokeWidth: 1,
              }}
            />
            <YAxis
              tickFormatter={DataFormatter}
              tick={{
                stroke: "gray",
                strokeWidth: 0,
              }}
            />
            <Legend
            iconType="circle"
            layout="horizontal"
            horizontalAlign="middle"
            align="center"
              wrapperStyle={{
                padding: 20,
               
              }}
            />
            <Bar dataKey="dose_1" name="Dose 1" fill="#5a8dee" barSize="20%" />
            <Bar dataKey="dose_2" name="Dose 2" fill="#f54394" barSize="20%" />
          </BarChart>
        </ResponsiveContainer>
        </div>
    )
}
export default VaccinationCoverage