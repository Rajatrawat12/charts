import { PieChart, Pie, Legend, Cell, ResponsiveContainer } from "recharts"
import "./VaccinationByAge.css"

const VaccinationByAge=(props)=>{
    const age=props.age
    console.log(props)
    return (
        <div className="vaccinationCoverageContainer">
            <h1 className="headingVaccination">Vaccination By Age</h1>
        <ResponsiveContainer width="100%" height={300}>
        <PieChart>
          <Pie
            cx="50%"
            cy="50%"
            data={age}
            startAngle={0}
            endAngle={360}
            dataKey="count"
          >
            <Cell name="18-44" fill=" #2cc6c6" />
            <Cell name="45-60" fill=" #a3df9f" />
            <Cell name="above" fill="#64c2a6" />
          </Pie>
          <Legend
            iconType="circle"
            layout="horizontal"
            horizontalAlign="middle"
            align="center"
            wrapperStyle={{
                padding: 20,
               
              }}
          />
        </PieChart>
      </ResponsiveContainer>
      </div>
    )
}
export default VaccinationByAge