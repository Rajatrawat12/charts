import logo from './logo.svg';
import VaccinationCoverage from './VaccinationCoverage';
import VaccinationByGender from './VacinationByGender';
import VaccinationByAge from './VaccinationByAge';



import './App.css';
import { useEffect, useState } from 'react';

const App = () => {
  const URL = "https://apis.ccbp.in/covid-vaccination-data";
  const[fulldata,setfulldata]=useState(null);
  const [data, setData] = useState(null);
  const [gender,setGender]= useState(null);
  const [age,setAge]= useState(null);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    fetch(URL, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((jsonData) => {
        // Store the data in the state variable 
        setfulldata(jsonData)
        setData(jsonData.last_7_days_vaccination);
        setGender(jsonData.vaccination_by_gender);
        setAge(jsonData.vaccination_by_age)
        console.log(data)
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  };

 

  return (
    <div className='dashboardContainer'>
    <div className='vaccinContainer'>
      <img  className="CowinLogo"src="https://assets.ccbp.in/frontend/react-js/cowin-logo.png "/>
      <h1 className='logoText'>Co-WIN</h1>
    </div>
    <h1 className='headingCowin'>CoWIN Vaccination in India</h1>
    {fulldata ?(
      <>
    <VaccinationCoverage data={data}/>
    <VaccinationByGender gender={gender}/>
    <VaccinationByAge age={age}/>
    </>
    ):(
      <div className="loader">Loading...</div>
    )}
  </div>
  );
};

export default App;

